part of 'home_bloc.dart';

@immutable
abstract class HomeState {
  List<UserModel> get users;
  List<TaskModel> get tasks;
  List<UserModel> get selectedUsers;
}

class InitialHomeState extends HomeState {
  List<UserModel> get users => const [];
  List<TaskModel> get tasks => const [];
  List<UserModel> get selectedUsers => const [];
}

class DataLoadedState extends HomeState {
  DataLoadedState({required this.users, required this.tasks});

  final List<UserModel> users;
  final List<TaskModel> tasks;
  List<UserModel> get selectedUsers => const [];
}

class UsersSelectedState extends HomeState {
  UsersSelectedState(
      {required this.users, required this.tasks, required this.selectedUsers});

  final List<UserModel> users;
  final List<TaskModel> tasks;
  final List<UserModel> selectedUsers;
}

class UsersUnselectedState extends HomeState {
  UsersUnselectedState(
      {required this.users, required this.tasks, required this.selectedUsers});

  final List<UserModel> users;
  final List<TaskModel> tasks;
  final List<UserModel> selectedUsers;
}
