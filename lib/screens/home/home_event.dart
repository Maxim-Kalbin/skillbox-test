part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class LoadDataEvent implements HomeEvent {}

class SelectUserEvent implements HomeEvent {
  SelectUserEvent({required this.selected});

  final UserModel selected;
}

class DeselectUserEvent implements HomeEvent {
  DeselectUserEvent({required this.deselected});

  final UserModel deselected;
}
