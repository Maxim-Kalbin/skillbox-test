import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:skillbox_test/models/task_model.dart';
import 'package:skillbox_test/models/user_model.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(InitialHomeState());

  final List<UserModel> users = [];
  final List<TaskModel> tasks = [];

  final List<UserModel> selectedUsers = [];

  bool loaded = false;

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is LoadDataEvent && loaded == false) {
      final dio = Dio();
      final usersJson =
          (await dio.get('https://jsonplaceholder.typicode.com/users/')).data;

      for (var user in usersJson) {
        users.add(UserModel.fromJSON(user));
      }

      final tasksJson =
          (await dio.get('https://jsonplaceholder.typicode.com/todos/')).data;

      for (var task in tasksJson) {
        tasks.add(TaskModel.fromJSON(task));
      }

      loaded = true;
      yield DataLoadedState(users: users, tasks: tasks);
    }

    if (event is SelectUserEvent) {
      final user = event.selected;
      selectedUsers.add(user);

      yield UsersSelectedState(
          users: users, tasks: tasks, selectedUsers: selectedUsers);
    }

    if (event is DeselectUserEvent) {
      final user = event.deselected;
      selectedUsers.remove(user);

      yield UsersUnselectedState(
          users: users, tasks: tasks, selectedUsers: selectedUsers);
    }
  }
}
