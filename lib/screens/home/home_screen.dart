import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skillbox_test/screens/chat/chat_screen.dart';
import 'package:skillbox_test/screens/home/home_bloc.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(),
      child: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          final bloc = BlocProvider.of<HomeBloc>(context);
          if (state is! InitialHomeState) {
            return DefaultTabController(
              initialIndex: 0,
              length: 2,
              child: Scaffold(
                appBar: AppBar(
                  title: Align(
                      alignment: Alignment.centerLeft,
                      child: const Text('Home')),
                  bottom: const TabBar(
                    tabs: <Widget>[
                      Tab(
                        icon: Icon(Icons.person),
                      ),
                      Tab(
                        icon: Icon(Icons.today),
                      ),
                    ],
                  ),
                ),
                body: TabBarView(
                  children: <Widget>[
                    ListView.builder(
                      itemBuilder: (context, index) {
                        final user = state.users[index];
                        final selected = state.selectedUsers.contains(user);
                        return ListTile(
                          tileColor:
                              selected ? Colors.lightBlue[100] : Colors.white,
                          onTap: () {
                            if (selected) {
                              bloc.add(DeselectUserEvent(deselected: user));
                            } else {
                              bloc.add(SelectUserEvent(selected: user));
                            }
                          },
                          leading: SizedBox(
                            width: 64,
                            height: 64,
                            child: Center(
                              child: Text(
                                '${user.firstName[0]}${user.lastName[0]}',
                                style: TextStyle(fontSize: 19),
                              ),
                            ),
                          ),
                          title: Text(user.firstName,
                              style: TextStyle(fontSize: 17)),
                          subtitle:
                              Text(user.email, style: TextStyle(fontSize: 15)),
                        );
                      },
                      itemCount: state.users.length,
                    ),
                    ListView.builder(
                      itemBuilder: (context, index) {
                        final task = state.tasks[index];
                        return ListTile(
                          tileColor: task.completed
                              ? Colors.lightGreenAccent[100]
                              : Colors.white,
                          title:
                              Text(task.title, style: TextStyle(fontSize: 17)),
                        );
                      },
                      itemCount: state.users.length,
                    ),
                  ],
                ),
                floatingActionButton: AnimatedOpacity(
                  opacity: state.selectedUsers.isNotEmpty ? 1 : 0,
                  duration: Duration(milliseconds: 150),
                  child: FloatingActionButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChatScreen(
                                    users: state.selectedUsers,
                                  )));
                    },
                    child: Icon(Icons.chat),
                  ),
                ),
              ),
            );
          } else {
            bloc.add(LoadDataEvent());
            return SizedBox.expand(
              child: Center(
                child: SizedBox(
                  width: 40,
                  height: 40,
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
