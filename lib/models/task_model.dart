class TaskModel {
  final String title;
  final bool completed;

  TaskModel.fromJSON(Map<String, dynamic> json)
      : title = json['title'],
        completed = json['completed'];
}
